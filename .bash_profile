# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
for file in ~/.{path,bash_prompt,exports,aliases,functions,git-completion}; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

export PATH=.:$PATH:/usr/bin:/usr/local/bin:~/bin

#color ant
ant () { command ant  -logger org.apache.tools.ant.listener.AnsiColorLogger "$@" | sed 's/2;//g' ; }

# CVS
CVSROOT=:pserver:dkochhei@prcvs.douglas.co.us:/home/cvs/repository;
export CVSROOT
export CVSEDITOR=vi
export CVS_RSH=ssh
export PATH=$PATH:/usr/local/Cellar/cvs/1.12.13/bin

# ANT
export ANT_HOME=~/development/projects/commonbuild/ant
export PATH=$PATH:$ANT_HOME/bin

# GRADLE
export GRADLE_HOME=~/development/misc/gradle-2.3
export PATH=$PATH:$GRADLE_HOME/bin

# JAVA
#JAVA150_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/1.5.0/Home/; export JAVA150_HOME
#--pointing java 6 to v7 for the moment
#export JAVA160_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home/
#export JAVA_HOME=$JAVA160_HOME
export JAVA180_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_65.jdk/Contents/Home
export JAVA170_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_75.jdk/Contents/Home
export JAVA160_HOME=$JAVA180_HOME
export JAVA170_HOME=$JAVA180_HOME
export JAVA_HOME=$JAVA180_HOME # >=== IMPORTANT: also reset in /etc/launchd.conf!!!! ===
export IDEA_JDK=$JAVA_HOME


# JBOSS -NOTE: USE EDITOR TO UPDATE FOR JB7!!!!!
#export JBOSS60_HOME=/Users/dkochhei/development/appserver/jboss-6.0.0.Final
#NOTE: TO PICK UP IN INTELLIJ SEE: http://stackoverflow.com/questions/135688/setting-environment-variables-in-os-x
export JBOSS71_HOME=/Users/dkochhei/development/appserver/jboss-as-7.1.0.Final
#export JBOSS_HOME=$JBOSS71_HOME # >=== IMPORTANT: also reset in /etc/launchd.conf!!!! ===
export WILDFLY10_HOME=/Users/dkochhei/development/appserver/wildfly-10.0.0.Final
export JBOSS_HOME=$WILDFLY10_HOME
#export JBOSS_HOME=$JBOSS71_HOME

export PATH=$PATH:$JAVA_HOME/bin
export PATH=$PATH:$JBOSS_HOME/bin # >=== IMPORTANT: also reset in /etc/launchd.conf!!!! (for IntelliJ)===
export PATH=$PATH:$WILDFLY10_HOME/bin

# ORACLE
export ORACLE_HOME=/Users/dkochhei/development/oracle/instantclient_10_2
export DYLD_LIBRARY_PATH=$ORACLE_HOME
export DYLD_LIBRARY_PATH
export LD_LIBRARY_PATH=$ORACLE_HOME:$LD_LIBRARY_PATH
export SQLPATH=$ORACLE_HOME
export TNS_ADMIN=$ORACLE_HOME
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1
export NLS_DATE_FORMAT='mm/dd/yyyy hh24:mi:ss'
export PATH=$PATH:$ORACLE_HOME

#MAVEN
export MAVEN_HOME=~/development/maven/apache-maven-3.2.2
export PATH=$PATH:$MAVEN_HOME/bin

# SOTAR ENV SETTINGS (use full path, no ~)
export MAVEN_PROPERTIES_DIR=/Users/dkochhei/development/projects/sotar/properties
export SOTAR_PUBLIC_PROPERTIES=/Users/dkochhei/development/projects/sotar-public/properties

# mercurial
export PATH=$PATH:~/development/mercurial-3.0-rc

# GIT - needed to override
export PATH=/usr/local/git/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:$PATH

#GIT AUTOCOMPLETE
#if [ -f ~/.git-completion.bash ]; then
#  . ~/.git-completion.bash
#fi

# CLASS STUFF
export PATH=$PATH:~/development/rest_class/Capstone/JAX-RS/Admin
export CATALINA_HOME=

test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

# FLUTTER
export PATH="$PATH:/Users/dkochhei/development/flutter/bin"

echo "$(fortune)"  | cowsay | lolcat 

# Turn off the zsh warning message
export BASH_SILENCE_DEPRECATION_WARNING=1

# NVM
export NVM_DIR="/Users/dkochhei/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

