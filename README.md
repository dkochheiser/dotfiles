# README - for .bash shell, not zsh #
Basic setup for my dot files for OS X stolen from various places. 
Includes:

* command line git auto complete
* colorization
* aliases, functions, and other useful 

# How do I get set up? #

## Dependencies ##

### iTerm 2 ###

* Download and set up [iTerm2](http://iterm2.com/)
* Recommend a dark theme

### Sublime ###

* Set up [Sublime](http://www.sublimetext.com/)
* [Atom](atom.io) is another great editor, if you use atom make sure to update the functions file to use atom instead of sublime

### Brew ###

1. Install [brew](http://brew.sh/)
1. ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
1. DO NOT INSTALL NODEJS, OR POSTGRES WITH BREW! This creates more problems than its worth. 
1. In the command line run: brew install tree After the install change to a different directory thats not too deep and type *tree* 


## Deployment instructions ##

1. Clone to wherever (preferably your development/projects directory)
1. Review the files, back up what you want
1. Copy all files to your home directory.
1. Restart
1. REVIEW THE NEW FUNCTIONS/ALIASES. 
1. Watch other lame developers drool over your awesome terminal window and commands